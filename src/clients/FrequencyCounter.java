package clients;

import symbolTable.I_ST;
import symbolTable.LinearProbingHashST;
import symbolTable.SeparateChainingHashST;
import symbolTable.SequentialSearchST;
import util.In;
import util.Out;
import util.StdIn;
import util.StdOut;
import util.*;

/******************************************************************************

 *  Compilation:  javac FrequencyCounter.java
 *  Execution:    java FrequencyCounter L < input.txt
 *  Dependencies: ST.java StdIn.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/31elementary/tnyTale.txt
 *                http://algs4.cs.princeton.edu/31elementary/tale.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig100K.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig300K.txt
 *                http://algs4.cs.princeton.edu/31elementary/leipzig1M.txt
 *
 *  Read in a list of words from standard input and print out
 *  the most frequently occurring word that has length greater than
 *  a given threshold.
 *
 *  % java FrequencyCounter 1 < tinyTale.txt
 *  it 10
 *
 *  % java FrequencyCounter 8 < tale.txt
 *  business 122
 *
 *  % java FrequencyCounter 10 < leipzig1M.txt
 *  government 24763
 *
 *
 ******************************************************************************/

/**
 *  The <tt>FrequencyCounter</tt> class provides a client for 
 *  reading in a sequence of words and printing a word (exceeding
 *  a given length) that occurs most frequently. It is useful as
 *  a test client for various symbol table implementations.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/31elementary">Section 3.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class FrequencyCounter {

    // Do not instantiate.
    private FrequencyCounter() { }

    /**
     * Reads in a command-line integer and sequence of words from
     * standard input and prints out a word (whose length exceeds
     * the threshold) that occurs most frequently to standard output.
     * It also prints out the number of words whose length exceeds
     * the threshold and the number of distinct such words.
     */
    public static void main(String[] args) {
        int distinct = 0, words = 0;
        int minlen = Integer.parseInt(args[0]);
       
        I_ST<String, Integer> st = new SequentialSearchST<String, Integer>();

        In in = new In(args[1]); //args[1] is the input file
        Out out = new Out(args[2]); //args[2] is the output file
                
        // compute frequency counts
        Stopwatch sw = new Stopwatch();
        while (in.hasNextLine()) {
            try
            {
	        	String key = in.readString();
	           
	            
	            if (key != null && key.length() < minlen) continue;
	            //System.out.println(key);
	            words++;
	            
	            // FILL IN
	            // if the symbol table contains the key
	            // increment the value of the symbol table entry
	            if (st.get(key)!=null) 
	            {
	            	st.put(key, st.get(key)+1);
	            }
	            else
	            {
	            	//FILL IN
	            	//add the key to the symbol table with a value of 1
	            	st.put(key, 1);
	                distinct++;
	            }
            }
            catch (java.util.NoSuchElementException e)
            {
            	break;
            }
        }
        
        // write the frequency counts to a file
        for (String word : st.keys())
		{
			out.println(word + " " + st.get(word));
		}

        // find a key with the highest frequency count
        String max = "";
        st.put(max, 0);
        for (String word : st.keys()) {
            if (st.get(word) > st.get(max))
                max = word;
        }

        StdOut.println(max + " " + st.get(max));
        StdOut.println("distinct = " + distinct);
        StdOut.println("words    = " + words);
        StdOut.println("Time elapsed: " + sw.elapsedTime());
    }
}
